extern crate futures;
extern crate hyper;
#[macro_use]
extern crate log;
extern crate net2;
extern crate pretty_env_logger;
extern crate regex;
extern crate tokio_core;

use futures::prelude::*;
use futures::future::*;
use futures::future;

use futures::Stream;
use net2::unix::UnixTcpBuilderExt;
use tokio_core::reactor::Core;
use tokio_core::net::TcpListener;
use std::thread;
use std::net::SocketAddr;
use std::sync::Arc;
use hyper::header::ContentLength;

use hyper::{Body, Chunk, Get, Method, Post, StatusCode};
use hyper::server::{Http, Request, Response, Service};
use futures::future::*;
use regex::*;

struct BenchService;

impl BenchService {
    fn fib(n: u64) -> u64 {
        let mut a = 0u64;
        let mut b = 1u64;
        for _ in 0..n {
            let tmp = a;
            a = b;
            b = a + tmp;
        }
        b
    }

    fn fib_recursive(n: u64) -> u64 {
        match n {
            0 => panic!("zero is not a right argument to fib_recursive()!"),
            1 | 2 => 1,
            3 => 2,
            _ => BenchService::fib_recursive(n - 1) + BenchService::fib_recursive(n - 2),
        }
    }
}

impl Service for BenchService {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = Box<futures::Future<Item = Self::Response, Error = Self::Error>>;

    fn call(&self, req: Request) -> Self::Future {
        match (req.method(), req.path()) {
            (&Method::Post, "/fib") => {
                let f = req.body()
                    .fold(Vec::new(), |mut acc, chunk| {
                        acc.extend_from_slice(&*chunk);
                        futures::future::ok::<_, Self::Error>(acc)
                    })
                    .map(|body| {
                        Response::new()
                            .with_body(
                                BenchService::fib(
                                    String::from_utf8(body)
                                        .unwrap()
                                        .parse::<u64>()
                                        .unwrap()
                                ).to_string()
                            )
                    });
                Box::new(f)
            }
            (&Method::Post, "/fib_recursive") => {
                let f = req.body()
                    .fold(Vec::new(), |mut acc, chunk| {
                        acc.extend_from_slice(&*chunk);
                        futures::future::ok::<_, Self::Error>(acc)
                    })
                    .map(|body| {
                        Response::new()
                            .with_body(
                                BenchService::fib_recursive(
                                    String::from_utf8(body)
                                        .unwrap()
                                        .parse::<u64>()
                                        .unwrap()
                                ).to_string()
                            )
                    });
                Box::new(f)
            }
            (&Method::Post, "/echo/reverse") => {
                let f = req.body()
                    .fold(Vec::new(), |mut acc, chunk| {
                        acc.extend_from_slice(&*chunk);
                        futures::future::ok::<_, Self::Error>(acc)
                    })
                    .map(|body| {
                        Response::new()
                            .with_body(body
                                .iter()
                                .rev()
                                .cloned()
                                .collect::<Vec<u8>>()
                            )
                    });
                Box::new(f)
            }
            _ => Box::new(futures::future::ok(
                Response::new().with_status(StatusCode::NotFound),
            )),
        }
    }
}


fn serve(addr: &SocketAddr, protocol: &Http) {
    let mut core = Core::new().unwrap();
    let handle = core.handle();
    let listener = net2::TcpBuilder::new_v4()
        .unwrap()
        .reuse_port(true)
        .unwrap()
        .bind(addr)
        .unwrap()
        .listen(1024)
        .unwrap();
    let listener = TcpListener::from_listener(listener, addr, &handle).unwrap();
    core.run(
        listener
            .incoming()
            .for_each(|(socket, addr)| {
                protocol.bind_connection(&handle, socket, addr, BenchService);
                Ok(())
            })
            .or_else(|e| -> FutureResult<(), ()> {
                panic!("TCP listener failed: {}", e);
            }),
    ).unwrap();
}

fn start_server(nb_instances: usize, addr: &str) {
    let addr = addr.parse().unwrap();

    let protocol = Arc::new(Http::new());
    {
        for _ in 0..nb_instances {
            let protocol = protocol.clone();
            thread::spawn(move || serve(&addr, &protocol));
        }
    }
    serve(&addr, &protocol);
}


fn main() {
    start_server(4, "127.0.0.1:8080");
}

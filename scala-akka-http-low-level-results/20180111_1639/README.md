# Environment

Local Virtualbox desktop. This test is not meant to be a serious indicator of performance as JMeter, the program under test and various other programs are all executing in the environment at the same time.

Scala version: 2.12.4

## Dependencies

```scala
val akkaHttpVersion = "10.0.11"
val akkaVersion = "2.5.8"
```

## JVM Settings

```scala
  "-XX:+UseG1GC",
  "-Djava.net.preferIPv4Stack=true",
  "-server",
  "-Xmx128M",
  "-Xms128M",
  "-XX:MaxGCPauseMillis=100",
  "-XX:ParallelGCThreads=20",
  "-XX:ConcGCThreads=5",
  "-XX:InitiatingHeapOccupancyPercent=70",
  "-XX:+HeapDumpOnOutOfMemoryError"
```

# Notes

* Run both fib and fib_recursive.
* Update dependency version.

Disclaimer: This is a work in progress. This work is for personal reference.

# About

This project provides a simple, non-scientific benchmark comparison between the low level Akka Http library for Scala and the Rust Hyper library.

# Motivation

I currently use Scala as my day to day programming language. I am evaluating Rust as an alternative to Scala for use in memory and cpu constrained environments. This performance benchmark was written to provide a point of reference for Scala and Rust performance (and scalibility) at various resource availability levels.

# Goals

I want to compare performance for the base HTTP stacks. 

I do not want to compare performance for json parsing, or for cases that can be looked up elsewhere, like the benchmarks at the [Benchmarksgame](http://benchmarksgame.alioth.debian.org/) site. As such I try and keep the performed tests as simple as possible, with as little as possible dependencies. 

# Technologies

## Rust stack

Rust version: stable

* Hyper

## Scala stack

Scala version: 2.12.x

* Akka Http

# Test Description

## Api

At the moment the test only takes into consideration simple mathematics and string operations.

| Method | URL           | Body                                           | Response               | Note                                                                                  |
| ------ | :------------ | ---------------------------------------------- | ---------------------- | ------------------------------------------------------------------------------------- |
| POST   | /fib          | The base number for the  fibonacci calculation | The calculated number. | Fibonacci is a bit of an lousy match for this, should be rewritten to something else. |
| POST   | /fib_recursive          | The base number for the  fibonacci calculation | The calculated number. | Fibonacci is a bit of an lousy match for this, should be rewritten to something else. Recursive version. |
| POST   | /echo/reverse | The string to echo.                            | The reversed string.   | The string should not be of trivial length.                                           |

## Test suite

Please refer to the included JMeter test suite.

## Test results

 Test results can be found in rust-hyper-results and scala-akka-low-level-results.
package aphyre

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Sink
import akka.util.ByteString

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContextExecutor, Future}

object AkkaImplicits {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher
  implicit val timeout: FiniteDuration = FiniteDuration(5000, MILLISECONDS)
}

object Service {
  import AkkaImplicits._

  def fib(n: Long): Long = {
    if (n == 0) {
      throw new IllegalStateException("zero is not a right argument to fib()!");
    } else if (n == 1) {
      return 1
    }
    var i = 0l
    var sum = 0l
    var last = 0l
    var curr = 1l
    while (i < n - 1) {
      sum = last + curr
      last = curr
      curr = sum
      i += 1l
    }
    sum
  }

  def fib_recursive(n: Long): Long = {
    n match {
      case 0     => throw new IllegalStateException("AAArrghh")
      case 1 | 2 => 1
      case 3     => 2
      case _     => fib_recursive(n - 1) + fib_recursive(n - 2)
    }
  }

  def requestHandler(httpRequest: HttpRequest): Future[HttpResponse] = httpRequest match {
    case HttpRequest(POST, uri, _, entity, _) =>
      val requestBody: Future[String] = entity.toStrict(timeout) flatMap { e =>
        e.dataBytes
          .runFold(ByteString.empty) { case (acc, b) => acc ++ b }
          .map(_.utf8String)
      }
      uri.path match {
        case Path("/fib") =>
          requestBody.map(data => HttpResponse(entity = fib(data.toLong).toString()))
        case Path("/fib_recursive") =>
          requestBody.map(data => HttpResponse(entity = fib_recursive(data.toLong).toString()))
        case Path("/echo/reverse") =>
          requestBody.map(data => HttpResponse(entity = data.reverse))
        case _ =>
         Future.successful(HttpResponse(404, entity = "Unknown resource!"))
      }
    case r: HttpRequest =>
      Future.successful {
        r.discardEntityBytes() // important to drain incoming HTTP Entity stream
        HttpResponse(404, entity = "Unknown resource!")
      }
  }
}

object Server {
  import AkkaImplicits._

  def main(args: Array[String]): Unit = {
    val serverSource = Http().bind(interface = "localhost", port = 8080)
    serverSource.to(Sink.foreach { connection =>
      connection handleWithAsyncHandler Service.requestHandler
    }).run()
  }
}

import sbt._
import sbt.Keys._

name := "akka-http-low-level"
organization  := "aphyre"
scalaVersion := "2.12.4"

fork in run := true
javaOptions in run ++= Seq(
  "-XX:+UseG1GC",
  "-Djava.net.preferIPv4Stack=true",
  //"-XX:+UseGCLogFileRotation",
  //"-XX:NumberOfGCLogFiles=5",
  //"-XX:GCLogFileSize=10M",
  //"-XX:+PrintGCDetails",
  //"-XX:+PrintGCDateStamps",
  "-server",
  "-Xmx128M",
  "-Xms128M",
  "-XX:MaxGCPauseMillis=100",
  "-XX:ParallelGCThreads=20",
  "-XX:ConcGCThreads=5",
  "-XX:InitiatingHeapOccupancyPercent=70",
  "-XX:+HeapDumpOnOutOfMemoryError"
)

val akkaHttpVersion = "10.0.11"
val akkaVersion = "2.5.8"

libraryDependencies ++= Seq(
  // akka Http
  "com.typesafe.akka"   %% "akka-http"              % akkaHttpVersion,
  "com.typesafe.akka"   %% "akka-http-core"         % akkaHttpVersion,
  "com.typesafe.akka"   %% "akka-stream"            % akkaVersion,

  // logging
  "ch.qos.logback"             %  "logback-classic"   % "1.1.7",
  "com.typesafe.scala-logging" %% "scala-logging"     % "3.7.1",
  "com.typesafe.akka"          %% "akka-slf4j"        % akkaVersion
)

enablePlugins(JavaServerAppPackaging)

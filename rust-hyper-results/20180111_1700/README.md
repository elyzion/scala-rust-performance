# Environment

Local Virtualbox desktop. This test is not meant to be a serious indicator of performance as JMeter, the program under test and various other programs are all executing in the environment at the same time.

Rust version: 1.23.0 (stable)

## Dependencies 

```toml
[dependencies]
tokio-core = "0.1.12"
futures = "0.1.17"
hyper = "0.11.12"
regex = "0.2.5"
log4rs = "0.8.0"
log = "0.4.1"
env_logger = "0.5.0-rc.2"
pretty_env_logger = "0.2.0-rc.2"
net2 = "0.2.31"
```

# Notes

* Update dependencies to newest versions.
